package com.dvcam.s50.pizza365.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="drinks")
public class CDrink {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long drink_id;
    @Column(name="ma_nuoc_uong")
    private String maNuocong;
    @Column(name="ten_nuoc_uong")
    private String tenNuocUong;
    @Column(name="gia_nuoc_uong")
    private long price;
    @Column(name="ngay_tao")
    private long ngayTao;
    @Column(name="nay_cap_nhat")
    private long ngayCapNhat;

    public CDrink() {
    }

    public CDrink(String maNuocong, String tenNuocUong, long price, long ngayTao, long ngayCapNhat) {
        this.maNuocong = maNuocong;
        this.tenNuocUong = tenNuocUong;
        this.price = price;
        this.ngayTao = ngayTao;
        this.ngayCapNhat = ngayCapNhat;
    }

    public long getId() {
        return this.drink_id;
    }

    public void setId(long drink_id) {
        this.drink_id = drink_id;
    }

    public String getMaNuocong() {
        return this.maNuocong;
    }

    public void setMaNuocong(String maNuocong) {
        this.maNuocong = maNuocong;
    }

    public String getTenNuocUong() {
        return this.tenNuocUong;
    }

    public void setTenNuocUong(String tenNuocUong) {
        this.tenNuocUong = tenNuocUong;
    }

    public long getPrice() {
        return this.price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public long getNgayTao() {
        return this.ngayTao;
    }

    public void setNgayTao(long ngayTao) {
        this.ngayTao = ngayTao;
    }

    public long getNgayCapNhat() {
        return this.ngayCapNhat;
    }

    public void setNgayCapNhat(long ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
    }




    
}

