package com.dvcam.s50.pizza365.controller;

import java.util.ArrayList;
import java.util.List;

import com.dvcam.s50.pizza365.model.CDrink;
import com.dvcam.s50.pizza365.reponsitory.IDrinkRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("/")
public class CDrinkController {
    @Autowired
    IDrinkRepository pCustomerRepository;

    @GetMapping("/drinks")
    public ResponseEntity<List<CDrink>> getAllCDrinks(){
        try {
            List<CDrink> listCustomer = new ArrayList<CDrink>();

            pCustomerRepository.findAll().forEach(listCustomer::add);
            return new ResponseEntity<>(listCustomer,HttpStatus.OK);
        } catch(Exception ex){

            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
}