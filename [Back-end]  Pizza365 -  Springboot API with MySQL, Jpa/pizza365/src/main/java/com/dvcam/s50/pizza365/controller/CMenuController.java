package com.dvcam.s50.pizza365.controller;

import java.util.ArrayList;
import java.util.List;

import com.dvcam.s50.pizza365.model.CMenu;
import com.dvcam.s50.pizza365.reponsitory.IMenuRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("/")
public class CMenuController {
    @Autowired
    IMenuRepository pIMenuRepository;

    @GetMapping("/menu")
     public ResponseEntity<List<CMenu>> getAllCDrinks(){
        try {
            List<CMenu> menuList = new ArrayList<CMenu>();

            pIMenuRepository.findAll().forEach(menuList::add);
            return new ResponseEntity<>(menuList,HttpStatus.OK);
        } catch(Exception ex){

            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
}
