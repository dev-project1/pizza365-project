package com.dvcam.s50.pizza365.controller;

import java.util.ArrayList;
import java.util.List;

import com.dvcam.s50.pizza365.model.CVoucher;
import com.dvcam.s50.pizza365.reponsitory.IVoucherRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
@RestController
@CrossOrigin
@RequestMapping("/")
public class CVoucherController {
    @Autowired
    IVoucherRepository vVoucherRespository;
    @GetMapping("/vouchers")
    public ResponseEntity<List<CVoucher>> getVouchers(){
        try{
            List<CVoucher> listVouchers = new ArrayList<CVoucher>();
            vVoucherRespository.findAll().forEach(listVouchers::add);
            return new ResponseEntity<>(listVouchers,HttpStatus.OK);

        }catch(Exception ex){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}