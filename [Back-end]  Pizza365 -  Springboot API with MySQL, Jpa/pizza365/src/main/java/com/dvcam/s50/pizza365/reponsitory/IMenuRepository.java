package com.dvcam.s50.pizza365.reponsitory;


import com.dvcam.s50.pizza365.model.CMenu;

import org.springframework.data.jpa.repository.JpaRepository;

public interface IMenuRepository extends JpaRepository<CMenu, Long>{

}
    