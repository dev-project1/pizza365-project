package com.dvcam.s50.pizza365.reponsitory;

import com.dvcam.s50.pizza365.model.CCustomer;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ICustomerRepository extends JpaRepository<CCustomer,Long>{
    CCustomer findById(long customer_id); // getId --> findBy  Id 
    
}
