package com.dvcam.s50.pizza365.reponsitory;

import com.dvcam.s50.pizza365.model.CDrink;

import org.springframework.data.jpa.repository.JpaRepository;

public interface IDrinkRepository extends JpaRepository<CDrink, Long>{
    
}
