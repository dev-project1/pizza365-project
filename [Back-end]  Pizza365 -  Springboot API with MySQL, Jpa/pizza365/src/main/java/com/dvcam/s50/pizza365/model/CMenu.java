package com.dvcam.s50.pizza365.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="menu")
public class CMenu {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long menu_id;
    @Column(name="size")
	private char size;
    @Column(name="duong_kinh")
	private long duongKinh;
    @Column(name="suon")
	private long suon;
    @Column(name="salad")
	private long salad;
    @Column(name="so_luong_nuoc")
	private long soLuongNuocNgot;
    @Column(name="price")
	private long price;

    
	
	public CMenu() {
    }

    public CMenu(char size, long duongKinh, long suon, long salad, long soLuongNuocNgot, long price) {
		this.size = size;
		this.duongKinh = duongKinh;
		this.suon = suon;
		this.salad = salad;
		this.soLuongNuocNgot = soLuongNuocNgot;
		this.price = price;
	}

	// method getter and setter

    public long getMenu_id() {
        return menu_id;
    }

    public void setMenu_id(long menu_id) {
        this.menu_id = menu_id;
    }
    
	public char getSize() {
		return size;
	}

    public void setSize(char size) {
		this.size = size;
	}

	public long getDuongKinh() {
		return duongKinh;
	}

	public void setDuongKinh(long duongKinh) {
		this.duongKinh = duongKinh;
	}

	public long getSuon() {
		return suon;
	}

	public void setSuon(long suon) {
		this.suon = suon;
	}

	public long getSalad() {
		return salad;
	}

	public void setSalad(long salad) {
		this.salad = salad;
	}

	public long getprice() {
		return price;
	}

	public void setprice(long price) {
		this.price = price;
	}

	public long getSoLuongNuocNgot() {
		return soLuongNuocNgot;
	}

	public void setSoLuongNuocNgot(long soLuongNuocNgot) {
		this.soLuongNuocNgot = soLuongNuocNgot;
	}

}


