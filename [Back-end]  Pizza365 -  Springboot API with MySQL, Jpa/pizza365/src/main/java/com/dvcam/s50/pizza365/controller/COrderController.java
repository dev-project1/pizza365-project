package com.dvcam.s50.pizza365.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.dvcam.s50.pizza365.model.CCustomer;
import com.dvcam.s50.pizza365.model.COrder;
import com.dvcam.s50.pizza365.reponsitory.ICustomerRepository;
import com.dvcam.s50.pizza365.reponsitory.IOrderRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;



@RestController
@CrossOrigin
@RequestMapping("/")
public class COrderController {
    @Autowired
    ICustomerRepository pICustomerReponsitory;
    @Autowired
    IOrderRepository pIOrderReponsitory;
    @GetMapping("/customers")
    public ResponseEntity <List<CCustomer>> getCustomerList(){
        try{
            List<CCustomer> userList = new ArrayList<>();
            pICustomerReponsitory.findAll().forEach(userList::add);
            return new ResponseEntity<>(userList,HttpStatus.OK);
        } catch(Exception e){
            System.out.println(e);
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/orders")
    public ResponseEntity <Set<COrder>> getOrderByCustomerId(@RequestParam(value="userId") long customer_id){
        try{
            CCustomer vCustomer = pICustomerReponsitory.findById(customer_id);
            if(vCustomer !=null){
                return new ResponseEntity<>(vCustomer.getOrders(),HttpStatus.OK);
            }else {
                return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
            }

            
        } catch(Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    
}