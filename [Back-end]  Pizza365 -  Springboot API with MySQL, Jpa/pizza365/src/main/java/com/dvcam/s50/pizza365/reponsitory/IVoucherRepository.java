package com.dvcam.s50.pizza365.reponsitory;

import com.dvcam.s50.pizza365.model.CVoucher;

import org.springframework.data.jpa.repository.JpaRepository;

public interface IVoucherRepository  extends JpaRepository<CVoucher,Long>{
    
}
