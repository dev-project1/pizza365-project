package com.dvcam.s50.pizza365.reponsitory;

import com.dvcam.s50.pizza365.model.COrder;

import org.springframework.data.jpa.repository.JpaRepository;

public interface IOrderRepository extends JpaRepository<COrder,Long>{
    
}
