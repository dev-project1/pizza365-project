package com.dvcam.s50.pizza365.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="vouchers")
public class CVoucher {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO) // id laf duy nhat tu tang thu tu
    private long voucher_id;
    @Column(name="phan_tram_giam_gia")
    private String phanTramGiamGia;
    @Column(name="ghi_chu")
    private String ghiChu;
    @Column(name="ma_voucher")
    private String maVoucher;
    @Column(name="ngay_tao")
	private long ngayTao;
    @Column(name="ngay_cap_nhat")
    private long ngayCapNhat;


    public CVoucher() {
    }

    public CVoucher(String phanTramGiamGia, String ghiChu, String maVoucher, long ngayTao, long ngayCapNhat) {
        
        this.phanTramGiamGia = phanTramGiamGia;
        this.ghiChu = ghiChu;
        this.maVoucher = maVoucher;
        this.ngayTao = ngayTao;
        this.ngayCapNhat = ngayCapNhat;
    }

    public long getId() {
        return this.voucher_id;
    }

    public void setId(long voucher_id) {
        this.voucher_id = voucher_id;
    }

    public String getPhanTramGiamGia() {
        return this.phanTramGiamGia;
    }

    public void setPhanTramGiamGia(String phanTramGiamGia) {
        this.phanTramGiamGia = phanTramGiamGia;
    }

    public String getGhiChu() {
        return this.ghiChu;
    }

    public void setGhiChu(String ghiChu) {
        this.ghiChu = ghiChu;
    }

    public String getMaVoucher() {
        return this.maVoucher;
    }

    public void setMaVoucher(String maVoucher) {
        this.maVoucher = maVoucher;
    }

    public long getNgayTao() {
        return this.ngayTao;
    }

    public void setNgayTao(long ngayTao) {
        this.ngayTao = ngayTao;
    }

    public long getNgayCapNhat() {
        return this.ngayCapNhat;
    }

    public void setNgayCapNhat(long ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
    }

   
    
}

